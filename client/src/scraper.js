const cheerio = require("cheerio");
const puppeteer = require("puppeteer");

const url = "https://www9.gogoanime.io/yichang-shengwu-jianwenlu-episode-12";

puppeteer
  .launch()
  .then(browser => browser.newPage())
  .then(page => {
    return page.goto(url).then(function() {
      return page.content();
    });
  })
  .then(html => {
    const $ = cheerio.load(html);
    const newsHeadlines = $('#load_anime .anime_video_body_watch_items .play-video iframe').attr('src');
    console.log(newsHeadlines);
  })
  .catch(console.error);
