import React from "react";
import AnimeList from "./AnimeList";

const AnimeLists = props => {
  let animeLists = props.lists.map(anime => {
    // let ep = anime.episodes.map(episode => {
    //   return (
    //     <li key={episode.id}>
    //       Episode {episode.attributes.number}:{" "}
    //       {episode.attributes.canonicalTitle}
    //     </li>
    //   );
    // });
    let title = anime.details.attributes.titles.en_us;
    if (!anime.details.attributes.titles.en_us) {
      title = anime.details.attributes.titles.en_jp;
    }
    let link = title
      .toString()
      .toLowerCase()
      .replace(/[^a-zA-Z0-9 ]/, "")
      .replace(/'/g, "")
      .replace(/\s/g, "-");
    let url = "https://www9.gogoanime.io/category/" + link;
    return (
      <AnimeList
        keyId={anime.details.id}
        url={url}
        title={title}
        episodes={anime.details.attributes.episodeCount}
        backgroundImage={anime.details.attributes.posterImage.small}
        desc={anime.details.attributes.titles.en}
      />
    );
  });

  return <ul className="Anime__lists">{animeLists}</ul>;
};

export default AnimeLists;
