import React from "react";

const AnimeList = props => {
  return (
    <li key={props.keyId} className="Anime__list">
      <div
        className="Anime__bg"
        style={{ backgroundImage: `url(${props.backgroundImage})` }}
      ></div>
      <div className="Anime__details">
        <p className="Anime__title">{props.title}</p>
        <p className="Anime__episodes">{props.episodes} Videos</p>
      </div>
    </li>
  );
};

export default AnimeList;
