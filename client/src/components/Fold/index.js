import React from "react";

const Fold = props => {
  let item = props.fold;
  return (
    <div
      className="Fold__bg"
      style={{
        backgroundImage: `url(${item.attributes.posterImage.original})`
      }}
    ></div>
  );
};

export default Fold;
