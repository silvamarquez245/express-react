import React, { Component } from "react";
import "../../css/styles.css";
import Wrapper from "../../components/Wrapper";
import Header from "../../components/Header";
import Fold from "../../components/Fold";
import AnimeLists from "../../components/AnimeLists";
class LandingPage extends Component {
  state = {
    error: null,
    item: [],
    streamLink: "",
    fold: "",
    loaded: false
  };

  componentDidMount() {
    this.episodeApi()
      .then(res => this.setState({ streamLink: "https:" + res }))
      .catch(err => console.log(err));

    this.randomApi()
      .then(result => {
        let item =
          result.data[Math.floor(Math.random() * [...result.data].length)];
        this.setState({
          fold: item,
          loaded: true
        });
      })
      .catch(err => console.log(err));

    fetch("https://kitsu.io/api/edge/anime?page[limit]=5")
      .then(res => res.json())
      .then(
        result => {
          return result.data.map(async r => {
            try {
              const res = await fetch(
                "https://kitsu.io/api/edge/anime/" + r.id + "/episodes"
              );
              const episodeResult = await res.json();
              this.setState({
                item: [
                  ...this.state.item,
                  {
                    details: r,
                    episodes: [...episodeResult.data],
                    loaded: true
                  }
                ]
              });
            } catch (error) {
              this.setState({
                error
              });
            }
          });
        },
        error => {
          this.setState({
            error
          });
        }
      );
  }
  episodeApi = async () => {
    const response = await fetch("/api/customers");
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  randomApi = async () => {
    const episode = await fetch("https://kitsu.io/api/edge/trending/anime");
    const body = await episode.json();
    if (episode.status !== 200) throw Error(body.message);
    return body;
  };

  render() {
    let animeState = this.state.item;
    let fold = this.state.fold;
    return (
      <main className="Anime">
        <Header />
        {this.state.loaded ? <Fold fold={fold} /> : (<p>Loading</p>)}
        <Wrapper>
        {this.state.loaded ? <AnimeLists lists={animeState} /> : (<p>Loading</p>)}
          
        </Wrapper>
      </main>
    );
  }
}

export default LandingPage;
