const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const bodyParser = require('body-parser');
const cheerio = require("cheerio");
const puppeteer = require("puppeteer");
const logger = require('morgan');
const cors = require('cors');

app.use(
  cors({
    origin: 'http://localhost:5000',
    credentials: true,
  })
);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/api/hello', (req, res) => {
  res.send({ express: 'Hello From Express' });
});

const url = "https://www9.gogoanime.io/yichang-shengwu-jianwenlu-episode-3";

let urlAnime = "";
puppeteer
  .launch()
  .then(browser => browser.newPage())
  .then(page => {
    return page.goto(url).then(function() {
      return page.content();
    });
  })
  .then(html => {
    const $ = cheerio.load(html);
    const newsHeadlines = $('#load_anime .anime_video_body_watch_items .play-video iframe').attr('src');
    urlAnime = newsHeadlines;
  })
  .catch(console.error);


app.get('/api/customers/', (req, res) => {
    let customers = urlAnime;
    res.json(customers);
});


app.listen(port, () => console.log(`Listening on port ${port}`));